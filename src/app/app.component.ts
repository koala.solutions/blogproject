import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  posts = [
  	{
  		title: 'Mon premier post',
  		content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel rhoncus lectus. Mauris nunc odio, fermentum ut fermentum et, posuere eu libero. Duis non elit in metus tempus gravida sed ac mi. Pellentesque quam orci, cursus eu viverra posuere, egestas pellentesque nibh.',
  		loveIts: 0,
  		createdAt: new Date('2019-01-18')
  	},
  	{
  		title: 'Mon deuxieme post',
  		content: 'Vestibulum sed mi non erat suscipit luctus ac nec dui. Sed sagittis sollicitudin tortor. Aliquam erat volutpat. Integer viverra arcu at lobortis faucibus. Vivamus aliquam auctor orci sit amet mollis. Fusce condimentum magna vel felis congue laoreet.',
  		loveIts: 0,
  		createdAt: new Date('2019-01-18')
  	},
  	{
  		title: 'Mon troisieme post',
  		content: 'Aliquam erat volutpat. Nullam ultricies massa pellentesque lorem ullamcorper egestas. Sed in tempus ex, vel rhoncus lorem. Curabitur magna nunc, mattis ut tincidunt et, dapibus et metus. Fusce gravida aliquet semper.',
  		loveIts: 0,
  		createdAt: new Date('2019-01-18')
  	},
  ]
}
