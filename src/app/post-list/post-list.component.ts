import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {
  @Input() title: string;
  @Input() content:string;
  @Input() createdAt:Date;
  @Input() loveIts:number;
  constructor() { }

  ngOnInit() {
  }
  
  like(){
  	this.loveIts++;
  }

  dislike(){
  	this.loveIts--;
  }
}
